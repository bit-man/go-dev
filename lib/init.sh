#!/usr/bin/env bash

function isGitRepo ()
{
    local folder=$1;
    [[ -d ${folder}/.git ]] && return 0;
    return 1
}

function isGitRepoUp() {
    local folder=$1

    while [[ $folder != "/" ]]; do
      isGitRepo $folder
      if [[ $? -eq 0 ]]; then
        echo $folder
        return
      fi
      folder=$(dirname $folder)
    done
    echo ""
}

function projectType() {
    local folder=$1

    [[ -f $folder/pom.xml ]] && echo MAVEN
    [[ -f $folder/build.gradle ]] && echo GRADLE
    [[ `find  $folder  -iname \*.xcodeproj -type d  | wc -l | tr -d ' '` -ne 0 ]] \
        && echo XCODE
}
