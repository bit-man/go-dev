#!/usr/bin/env bash

thisFolder=`dirname $0`

source $thisFolder/../lib/init.sh

function testNoHomeFolderProject() {
    isGitRepo $thisFolder
    assertEquals $? 1
}

function testHomeFolderProject() {
    isGitRepo $thisFolder/..
    assertEquals $? 0
}

function testHomeFolderProjectPath() {
    homeFolderProjectPath=`isGitRepoUp $thisFolder`
    expected=`dirname $thisFolder`
    assertEquals $expected $homeFolderProjectPath
}

function testDetectMavenProject() {
    projectType=`projectType $thisFolder/data/project/maven`
    assertEquals MAVEN $projectType
}

function testDetectGradleProject() {
    projectType=`projectType $thisFolder/data/project/gradle`
    assertEquals GRADLE $projectType
}

function testDetectXCodeProject() {
    projectType=`projectType $thisFolder/data/project/xcode`
    assertEquals XCODE $projectType
}

. shunit2
